//
//  ConsolesTableViewController.swift
//  MyGame
//
//  Created by aluno on 19/02/19.
//  Copyright © 2019 cesar. All rights reserved.
//

import UIKit

class ConsolesTableViewController: UITableViewController{

    
    var consolesManager = ConsolesManager.shared
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadConsoles()
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        loadConsoles()
    }
    
    
    
    func loadConsoles(){
        consolesManager.loadConsoles(with: context)
        tableView.reloadData()
    }
    
    

    
    
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return consolesManager.consoles.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellConsole", for: indexPath)
        let console = consolesManager.consoles[indexPath.row]
        cell.textLabel?.text = console.name
        return cell
    }
    
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let console = consolesManager.consoles[indexPath.row]
//        showAlert(with: console)    aqui editava
        tableView.deselectRow(at: indexPath, animated: false)
    }
    

    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            consolesManager.deleteConsole(index: indexPath.row, context: context)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier! == "segueEdit" {
            print("segueEdit")
            let vc = segue.destination as! ConsoleViewController
            
            vc.myConsole = consolesManager.consoles[tableView.indexPathForSelectedRow!.row]
        }
        else if segue.identifier! == "segueInsert" {}
    }

}
