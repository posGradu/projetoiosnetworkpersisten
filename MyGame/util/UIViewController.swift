//
//  UIViewController.swift
//  MyGame
//
//  Created by aluno on 20/02/19.
//  Copyright © 2019 cesar. All rights reserved.
//

import Foundation
import UIKit
import CoreData
extension UIViewController {
    
    // propriedade computada que através de uma Category permite agora que qualquer
    // objeto UIViewController conheça essa propriedade context.
    
    var context: NSManagedObjectContext {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        // obtem a instancia do Core Data stack
        return appDelegate.persistentContainer.viewContext
    }
}
